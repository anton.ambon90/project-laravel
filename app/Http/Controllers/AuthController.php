<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view("halaman.Register");
    }

   public function kirim(Request $request){
        //    dd($request->all());
        $nama_depan = $request->firstname;
        $nama_belakang = $request->lastname;
        $gender_l = $request->gender_l;
        $gender_p = $request->gender_p;
        $nama_kota = $request->Nama_Kota;
        $bahasa = $request->language;
        $biodata = $request->biodata;

        return view("halaman.Welcome", compact("nama_depan", "nama_belakang", "gender_l", "gender_p", "nama_kota", "bahasa", "biodata"));


   }
}