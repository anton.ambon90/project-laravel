<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "IndexController@home");
Route::get('/register', "AuthController@form");
Route::post('/welcome', "AuthController@kirim");

Route::get('/table', function(){
    return view('table.table');
});

Route::get('/data-table', function(){
    return view('table.data-table');
});

// CRUD Cast Builder

// Route::get('/cast/create', "CastBuilderController@create");
// Route::post('/cast', "CastBuilderController@store");
// Route::get('/cast', "CastBuilderController@index");
// Route::get('/cast/{cast_id}', "CastBuilderController@show");
// Route::get('/cast/{cast_id}/edit', "CastBuilderController@edit");
// Route::put('/cast/{cast_id}', "CastBuilderController@update");
// Route::delete('/cast/{cast_id}', "CastBuilderController@destroy");


// CRUD Cast Eloquient

Route::resource('cast', "CastController");