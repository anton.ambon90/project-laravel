@extends('layout.master')

@section('judul-halaman')
    Register
@endsection
@section('judul')
    Buat Account Baru!
@endsection

@section('judul-content')
    Sign Up Form
@endsection

@section('content')
<div>
    <!-- Membuat Form -->
    <form action="/welcome" method="post">
        @csrf
        <p>First name :</p>
        <input name="firstname" type="text">
        <p>Last name :</p>
        <input name="lastname" type="text">
        <p>Gender</p>
        <input type="radio" name="gender_l" value="Laki-Laki">Male <br>
        <input type="radio" name="gender_p" value="Perempuan">Female
        <p>Nationality</p>
        <select name="Nama_Kota">
            <option value="Indonesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
        </select>
        <p>Language Spoken</p>
        <input type="checkbox" name="language" value="Bahasa Indonesia">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="English">English <br>
        <input type="checkbox" name="language" value="other">Other
        <p>Bio</p>
        <textarea name="biodata" cols="30" rows="10"></textarea> <br>
        <button type="submit">Sign Up</button>
    </form>


</div>
@endsection
    
